﻿using System;
using System.IO;
using System.Threading;

namespace M09UF2E1_OlidAlejandro
{
    class ThreadProgram
    {
        static void Main()
        {
            RunProgram();
        }

        private static void RunProgram()
        {
            var path = Directory.GetCurrentDirectory();
            var pathN = path + @"..\..\..\..\Texto.txt";
            var pathL = path + @"..\..\..\..\TextoGrande.txt";
            var pathS = path + @"..\..\..\..\TextoChico.txt";

            Thread myNewThreadPath = new Thread(() => CountTxt(pathN));
            myNewThreadPath.Start();

            Thread myNewThreadPathL = new Thread(() => CountTxt(pathL));
            myNewThreadPathL.Start();
            
            Thread myNewThreadPathS = new Thread(() => CountTxt(pathS));
            myNewThreadPathS.Start();
        }

        static void CountTxt(object o)
        {
            int counter = 0;

            foreach (string line in File.ReadLines(o.ToString()))
            {
                counter++;
            }

            Console.WriteLine("Lineas: " + counter);
        }
    }
}
